The <code>stream-mining</code> Project
======================================

This project is an extension to the *streams* library that provides a
collection of online stream learning algorithms, such as classifiers,
regression algorithms or counting implementations.

The implementations/processors provided within this project form a toolbox
for applying as well as evaluating machine learning implementations on
large data sets in a streaming fashion.

