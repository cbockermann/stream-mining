/**
 * 
 */
package stream.data.stats;

import stream.data.Statistics;

/**
 * @author chris
 * 
 */
public interface StatisticsListener {

	public void dataArrived(Statistics statistics);
}
