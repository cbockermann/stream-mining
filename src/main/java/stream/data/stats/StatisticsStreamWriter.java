/**
 * 
 */
package stream.data.stats;

import java.io.OutputStream;

import stream.io.DataStreamWriter;

/**
 * @author chris
 * 
 */
public class StatisticsStreamWriter extends DataStreamWriter {

	public StatisticsStreamWriter(OutputStream out) {
		super(out);
	}
}
