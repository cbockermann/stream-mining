/**
 * 
 */
package stream.io;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AccessLogAuditReader;

import stream.Processor;
import stream.data.Data;

/**
 * <p>
 * This class implements a simple access-log data stream. It provides access to
 * log-data stored in the default Apache access log format.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AccessLogStream implements DataStream {

	AccessLogAuditReader reader;
	AuditEvent pending = null;
	Map<String, Class<?>> attributes = null;

	/**
	 * @param url
	 * @throws Exception
	 */
	public AccessLogStream(URL url) throws Exception {
		reader = new AccessLogAuditReader(url.openStream());
		pending = reader.readNext();

	}

	/**
	 * @see stream.io.DataStream#getAttributes()
	 */
	@Override
	public Map<String, Class<?>> getAttributes() {
		if (attributes != null)
			return attributes;

		try {
			attributes = new LinkedHashMap<String, Class<?>>();
			pending = reader.readNext();
			for (String var : pending.getVariables())
				attributes.put(var.toUpperCase(), String.class);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return attributes;
	}

	/**
	 * @see stream.io.DataStream#readNext()
	 */
	@Override
	public Data readNext() throws Exception {

		if (pending != null) {
			Data data = new LogData(pending);
			pending = null;
			return data;
		}

		AuditEvent evt = reader.readNext();
		return new LogData(evt);
	}

	public Data readNext(Data data) throws Exception {
		return readNext();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.io.DataStream#init()
	 */
	@Override
	public void init() throws Exception {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.io.DataStream#close()
	 */
	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.io.DataStream#addPreprocessor(stream.Processor)
	 */
	@Override
	public void addPreprocessor(Processor proc) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.io.DataStream#addPreprocessor(int, stream.Processor)
	 */
	@Override
	public void addPreprocessor(int idx, Processor proc) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stream.io.DataStream#getPreprocessors()
	 */
	@Override
	public List<Processor> getPreprocessors() {
		// TODO Auto-generated method stub
		return null;
	}
}